﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// UnityEngine.WaitForEndOfFrame
struct WaitForEndOfFrame_t2372756133;
// UnityEngine.WaitForFixedUpdate
struct WaitForFixedUpdate_t2130080621;
// UnityEngine.WaitForSeconds
struct WaitForSeconds_t1615819279;
// UnityEngine.WebCamTexture
struct WebCamTexture_t1290350902;
// UnityEngine.WebCamDevice[]
struct WebCamDeviceU5BU5D_t3721690872;
// UnityEngine.WrapperlessIcall
struct WrapperlessIcall_t2494346367;
// UnityEngine.WritableAttribute
struct WritableAttribute_t2171443922;
// UnityEngine.YieldInstruction
struct YieldInstruction_t2048002629;
// UnityEngineInternal.GenericStack
struct GenericStack_t931085639;
// UnityEngineInternal.TypeInferenceRuleAttribute
struct TypeInferenceRuleAttribute_t1657757719;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array1146569071.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "UnityEngine_UnityEngine_Vector34282066566MethodDeclarations.h"
#include "mscorlib_System_Void2863195528.h"
#include "mscorlib_System_Single4291918972.h"
#include "mscorlib_System_Int321153838500.h"
#include "mscorlib_System_IndexOutOfRangeException3456360697MethodDeclarations.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_IndexOutOfRangeException3456360697.h"
#include "mscorlib_System_Single4291918972MethodDeclarations.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_Boolean476798718.h"
#include "UnityEngine_UnityEngine_UnityString3369712284MethodDeclarations.h"
#include "mscorlib_ArrayTypes.h"
#include "UnityEngine_UnityEngine_Mathf4203372500MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector44282066567.h"
#include "UnityEngine_UnityEngine_Vector44282066567MethodDeclarations.h"
#include "UnityEngine_UnityEngine_WaitForEndOfFrame2372756133.h"
#include "UnityEngine_UnityEngine_WaitForEndOfFrame2372756133MethodDeclarations.h"
#include "UnityEngine_UnityEngine_YieldInstruction2048002629MethodDeclarations.h"
#include "UnityEngine_UnityEngine_WaitForFixedUpdate2130080621.h"
#include "UnityEngine_UnityEngine_WaitForFixedUpdate2130080621MethodDeclarations.h"
#include "UnityEngine_UnityEngine_WaitForSeconds1615819279.h"
#include "UnityEngine_UnityEngine_WaitForSeconds1615819279MethodDeclarations.h"
#include "UnityEngine_UnityEngine_WebCamDevice3274004757.h"
#include "UnityEngine_UnityEngine_WebCamDevice3274004757MethodDeclarations.h"
#include "UnityEngine_UnityEngine_WebCamTexture1290350902.h"
#include "UnityEngine_UnityEngine_WebCamTexture1290350902MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Texture2526458961MethodDeclarations.h"
#include "mscorlib_System_String7231557MethodDeclarations.h"
#include "UnityEngine_ArrayTypes.h"
#include "UnityEngine_UnityEngine_WrapperlessIcall2494346367.h"
#include "UnityEngine_UnityEngine_WrapperlessIcall2494346367MethodDeclarations.h"
#include "mscorlib_System_Attribute2523058482MethodDeclarations.h"
#include "UnityEngine_UnityEngine_WritableAttribute2171443922.h"
#include "UnityEngine_UnityEngine_WritableAttribute2171443922MethodDeclarations.h"
#include "UnityEngine_UnityEngine_YieldInstruction2048002629.h"
#include "mscorlib_System_Object4170816371MethodDeclarations.h"
#include "UnityEngine_UnityEngineInternal_GenericStack931085639.h"
#include "UnityEngine_UnityEngineInternal_GenericStack931085639MethodDeclarations.h"
#include "mscorlib_System_Collections_Stack1761758306MethodDeclarations.h"
#include "UnityEngine_UnityEngineInternal_MathfInternal4096243933.h"
#include "UnityEngine_UnityEngineInternal_MathfInternal4096243933MethodDeclarations.h"
#include "UnityEngine_UnityEngineInternal_TypeInferenceRuleA1657757719.h"
#include "UnityEngine_UnityEngineInternal_TypeInferenceRuleA1657757719MethodDeclarations.h"
#include "UnityEngine_UnityEngineInternal_TypeInferenceRules2889237774.h"
#include "mscorlib_System_Enum2862688501MethodDeclarations.h"
#include "mscorlib_System_Enum2862688501.h"
#include "UnityEngine_UnityEngineInternal_TypeInferenceRules2889237774MethodDeclarations.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single,System.Single)
extern "C"  void Vector3__ctor_m2926210380 (Vector3_t4282066566 * __this, float ___x0, float ___y1, float ___z2, const MethodInfo* method)
{
	{
		float L_0 = ___x0;
		__this->set_x_1(L_0);
		float L_1 = ___y1;
		__this->set_y_2(L_1);
		float L_2 = ___z2;
		__this->set_z_3(L_2);
		return;
	}
}
extern "C"  void Vector3__ctor_m2926210380_AdjustorThunk (Il2CppObject * __this, float ___x0, float ___y1, float ___z2, const MethodInfo* method)
{
	Vector3_t4282066566 * _thisAdjusted = reinterpret_cast<Vector3_t4282066566 *>(__this + 1);
	Vector3__ctor_m2926210380(_thisAdjusted, ___x0, ___y1, ___z2, method);
}
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single)
extern "C"  void Vector3__ctor_m1846874791 (Vector3_t4282066566 * __this, float ___x0, float ___y1, const MethodInfo* method)
{
	{
		float L_0 = ___x0;
		__this->set_x_1(L_0);
		float L_1 = ___y1;
		__this->set_y_2(L_1);
		__this->set_z_3((0.0f));
		return;
	}
}
extern "C"  void Vector3__ctor_m1846874791_AdjustorThunk (Il2CppObject * __this, float ___x0, float ___y1, const MethodInfo* method)
{
	Vector3_t4282066566 * _thisAdjusted = reinterpret_cast<Vector3_t4282066566 *>(__this + 1);
	Vector3__ctor_m1846874791(_thisAdjusted, ___x0, ___y1, method);
}
// System.Single UnityEngine.Vector3::get_Item(System.Int32)
extern Il2CppClass* IndexOutOfRangeException_t3456360697_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral965278184;
extern const uint32_t Vector3_get_Item_m108333500_MetadataUsageId;
extern "C"  float Vector3_get_Item_m108333500 (Vector3_t4282066566 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Vector3_get_Item_m108333500_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___index0;
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0019;
		}
		if (L_1 == 1)
		{
			goto IL_0020;
		}
		if (L_1 == 2)
		{
			goto IL_0027;
		}
	}
	{
		goto IL_002e;
	}

IL_0019:
	{
		float L_2 = __this->get_x_1();
		return L_2;
	}

IL_0020:
	{
		float L_3 = __this->get_y_2();
		return L_3;
	}

IL_0027:
	{
		float L_4 = __this->get_z_3();
		return L_4;
	}

IL_002e:
	{
		IndexOutOfRangeException_t3456360697 * L_5 = (IndexOutOfRangeException_t3456360697 *)il2cpp_codegen_object_new(IndexOutOfRangeException_t3456360697_il2cpp_TypeInfo_var);
		IndexOutOfRangeException__ctor_m1621772274(L_5, _stringLiteral965278184, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}
}
extern "C"  float Vector3_get_Item_m108333500_AdjustorThunk (Il2CppObject * __this, int32_t ___index0, const MethodInfo* method)
{
	Vector3_t4282066566 * _thisAdjusted = reinterpret_cast<Vector3_t4282066566 *>(__this + 1);
	return Vector3_get_Item_m108333500(_thisAdjusted, ___index0, method);
}
// UnityEngine.Vector3 UnityEngine.Vector3::Cross(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Vector3_t4282066566  Vector3_Cross_m2894122475 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___lhs0, Vector3_t4282066566  ___rhs1, const MethodInfo* method)
{
	{
		float L_0 = (&___lhs0)->get_y_2();
		float L_1 = (&___rhs1)->get_z_3();
		float L_2 = (&___lhs0)->get_z_3();
		float L_3 = (&___rhs1)->get_y_2();
		float L_4 = (&___lhs0)->get_z_3();
		float L_5 = (&___rhs1)->get_x_1();
		float L_6 = (&___lhs0)->get_x_1();
		float L_7 = (&___rhs1)->get_z_3();
		float L_8 = (&___lhs0)->get_x_1();
		float L_9 = (&___rhs1)->get_y_2();
		float L_10 = (&___lhs0)->get_y_2();
		float L_11 = (&___rhs1)->get_x_1();
		Vector3_t4282066566  L_12;
		memset(&L_12, 0, sizeof(L_12));
		Vector3__ctor_m2926210380(&L_12, ((float)((float)((float)((float)L_0*(float)L_1))-(float)((float)((float)L_2*(float)L_3)))), ((float)((float)((float)((float)L_4*(float)L_5))-(float)((float)((float)L_6*(float)L_7)))), ((float)((float)((float)((float)L_8*(float)L_9))-(float)((float)((float)L_10*(float)L_11)))), /*hidden argument*/NULL);
		return L_12;
	}
}
// System.Int32 UnityEngine.Vector3::GetHashCode()
extern "C"  int32_t Vector3_GetHashCode_m3912867704 (Vector3_t4282066566 * __this, const MethodInfo* method)
{
	{
		float* L_0 = __this->get_address_of_x_1();
		int32_t L_1 = Single_GetHashCode_m65342520(L_0, /*hidden argument*/NULL);
		float* L_2 = __this->get_address_of_y_2();
		int32_t L_3 = Single_GetHashCode_m65342520(L_2, /*hidden argument*/NULL);
		float* L_4 = __this->get_address_of_z_3();
		int32_t L_5 = Single_GetHashCode_m65342520(L_4, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)((int32_t)((int32_t)L_1^(int32_t)((int32_t)((int32_t)L_3<<(int32_t)2))))^(int32_t)((int32_t)((int32_t)L_5>>(int32_t)2))));
	}
}
extern "C"  int32_t Vector3_GetHashCode_m3912867704_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Vector3_t4282066566 * _thisAdjusted = reinterpret_cast<Vector3_t4282066566 *>(__this + 1);
	return Vector3_GetHashCode_m3912867704(_thisAdjusted, method);
}
// System.Boolean UnityEngine.Vector3::Equals(System.Object)
extern Il2CppClass* Vector3_t4282066566_il2cpp_TypeInfo_var;
extern const uint32_t Vector3_Equals_m3337192096_MetadataUsageId;
extern "C"  bool Vector3_Equals_m3337192096 (Vector3_t4282066566 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Vector3_Equals_m3337192096_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Vector3_t4282066566  V_0;
	memset(&V_0, 0, sizeof(V_0));
	int32_t G_B6_0 = 0;
	{
		Il2CppObject * L_0 = ___other0;
		if (((Il2CppObject *)IsInstSealed(L_0, Vector3_t4282066566_il2cpp_TypeInfo_var)))
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}

IL_000d:
	{
		Il2CppObject * L_1 = ___other0;
		V_0 = ((*(Vector3_t4282066566 *)((Vector3_t4282066566 *)UnBox (L_1, Vector3_t4282066566_il2cpp_TypeInfo_var))));
		float* L_2 = __this->get_address_of_x_1();
		float L_3 = (&V_0)->get_x_1();
		bool L_4 = Single_Equals_m2110115959(L_2, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0056;
		}
	}
	{
		float* L_5 = __this->get_address_of_y_2();
		float L_6 = (&V_0)->get_y_2();
		bool L_7 = Single_Equals_m2110115959(L_5, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0056;
		}
	}
	{
		float* L_8 = __this->get_address_of_z_3();
		float L_9 = (&V_0)->get_z_3();
		bool L_10 = Single_Equals_m2110115959(L_8, L_9, /*hidden argument*/NULL);
		G_B6_0 = ((int32_t)(L_10));
		goto IL_0057;
	}

IL_0056:
	{
		G_B6_0 = 0;
	}

IL_0057:
	{
		return (bool)G_B6_0;
	}
}
extern "C"  bool Vector3_Equals_m3337192096_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	Vector3_t4282066566 * _thisAdjusted = reinterpret_cast<Vector3_t4282066566 *>(__this + 1);
	return Vector3_Equals_m3337192096(_thisAdjusted, ___other0, method);
}
// UnityEngine.Vector3 UnityEngine.Vector3::Normalize(UnityEngine.Vector3)
extern "C"  Vector3_t4282066566  Vector3_Normalize_m3047997355 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___value0, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		Vector3_t4282066566  L_0 = ___value0;
		float L_1 = Vector3_Magnitude_m995314358(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		float L_2 = V_0;
		if ((!(((float)L_2) > ((float)(1.0E-05f)))))
		{
			goto IL_001a;
		}
	}
	{
		Vector3_t4282066566  L_3 = ___value0;
		float L_4 = V_0;
		Vector3_t4282066566  L_5 = Vector3_op_Division_m4277988370(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		return L_5;
	}

IL_001a:
	{
		Vector3_t4282066566  L_6 = Vector3_get_zero_m2017759730(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_6;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::get_normalized()
extern "C"  Vector3_t4282066566  Vector3_get_normalized_m2650940353 (Vector3_t4282066566 * __this, const MethodInfo* method)
{
	{
		Vector3_t4282066566  L_0 = Vector3_Normalize_m3047997355(NULL /*static, unused*/, (*(Vector3_t4282066566 *)__this), /*hidden argument*/NULL);
		return L_0;
	}
}
extern "C"  Vector3_t4282066566  Vector3_get_normalized_m2650940353_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Vector3_t4282066566 * _thisAdjusted = reinterpret_cast<Vector3_t4282066566 *>(__this + 1);
	return Vector3_get_normalized_m2650940353(_thisAdjusted, method);
}
// System.String UnityEngine.Vector3::ToString()
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern Il2CppClass* Single_t4291918972_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1732534255;
extern const uint32_t Vector3_ToString_m3566373060_MetadataUsageId;
extern "C"  String_t* Vector3_ToString_m3566373060 (Vector3_t4282066566 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Vector3_ToString_m3566373060_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t1108656482* L_0 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)3));
		float L_1 = __this->get_x_1();
		float L_2 = L_1;
		Il2CppObject * L_3 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_3);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_3);
		ObjectU5BU5D_t1108656482* L_4 = L_0;
		float L_5 = __this->get_y_2();
		float L_6 = L_5;
		Il2CppObject * L_7 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 1);
		ArrayElementTypeCheck (L_4, L_7);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_7);
		ObjectU5BU5D_t1108656482* L_8 = L_4;
		float L_9 = __this->get_z_3();
		float L_10 = L_9;
		Il2CppObject * L_11 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 2);
		ArrayElementTypeCheck (L_8, L_11);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)L_11);
		String_t* L_12 = UnityString_Format_m427603113(NULL /*static, unused*/, _stringLiteral1732534255, L_8, /*hidden argument*/NULL);
		return L_12;
	}
}
extern "C"  String_t* Vector3_ToString_m3566373060_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Vector3_t4282066566 * _thisAdjusted = reinterpret_cast<Vector3_t4282066566 *>(__this + 1);
	return Vector3_ToString_m3566373060(_thisAdjusted, method);
}
// System.Single UnityEngine.Vector3::Dot(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  float Vector3_Dot_m2370485424 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___lhs0, Vector3_t4282066566  ___rhs1, const MethodInfo* method)
{
	{
		float L_0 = (&___lhs0)->get_x_1();
		float L_1 = (&___rhs1)->get_x_1();
		float L_2 = (&___lhs0)->get_y_2();
		float L_3 = (&___rhs1)->get_y_2();
		float L_4 = (&___lhs0)->get_z_3();
		float L_5 = (&___rhs1)->get_z_3();
		return ((float)((float)((float)((float)((float)((float)L_0*(float)L_1))+(float)((float)((float)L_2*(float)L_3))))+(float)((float)((float)L_4*(float)L_5))));
	}
}
// System.Single UnityEngine.Vector3::Angle(UnityEngine.Vector3,UnityEngine.Vector3)
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern const uint32_t Vector3_Angle_m1904328934_MetadataUsageId;
extern "C"  float Vector3_Angle_m1904328934 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___from0, Vector3_t4282066566  ___to1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Vector3_Angle_m1904328934_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Vector3_t4282066566  L_0 = Vector3_get_normalized_m2650940353((&___from0), /*hidden argument*/NULL);
		Vector3_t4282066566  L_1 = Vector3_get_normalized_m2650940353((&___to1), /*hidden argument*/NULL);
		float L_2 = Vector3_Dot_m2370485424(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_3 = Mathf_Clamp_m3872743893(NULL /*static, unused*/, L_2, (-1.0f), (1.0f), /*hidden argument*/NULL);
		float L_4 = acosf(L_3);
		return ((float)((float)L_4*(float)(57.29578f)));
	}
}
// System.Single UnityEngine.Vector3::Magnitude(UnityEngine.Vector3)
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern const uint32_t Vector3_Magnitude_m995314358_MetadataUsageId;
extern "C"  float Vector3_Magnitude_m995314358 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___a0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Vector3_Magnitude_m995314358_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = (&___a0)->get_x_1();
		float L_1 = (&___a0)->get_x_1();
		float L_2 = (&___a0)->get_y_2();
		float L_3 = (&___a0)->get_y_2();
		float L_4 = (&___a0)->get_z_3();
		float L_5 = (&___a0)->get_z_3();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_6 = sqrtf(((float)((float)((float)((float)((float)((float)L_0*(float)L_1))+(float)((float)((float)L_2*(float)L_3))))+(float)((float)((float)L_4*(float)L_5)))));
		return L_6;
	}
}
// System.Single UnityEngine.Vector3::get_magnitude()
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern const uint32_t Vector3_get_magnitude_m989985786_MetadataUsageId;
extern "C"  float Vector3_get_magnitude_m989985786 (Vector3_t4282066566 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Vector3_get_magnitude_m989985786_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = __this->get_x_1();
		float L_1 = __this->get_x_1();
		float L_2 = __this->get_y_2();
		float L_3 = __this->get_y_2();
		float L_4 = __this->get_z_3();
		float L_5 = __this->get_z_3();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_6 = sqrtf(((float)((float)((float)((float)((float)((float)L_0*(float)L_1))+(float)((float)((float)L_2*(float)L_3))))+(float)((float)((float)L_4*(float)L_5)))));
		return L_6;
	}
}
extern "C"  float Vector3_get_magnitude_m989985786_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Vector3_t4282066566 * _thisAdjusted = reinterpret_cast<Vector3_t4282066566 *>(__this + 1);
	return Vector3_get_magnitude_m989985786(_thisAdjusted, method);
}
// System.Single UnityEngine.Vector3::SqrMagnitude(UnityEngine.Vector3)
extern "C"  float Vector3_SqrMagnitude_m1662776270 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___a0, const MethodInfo* method)
{
	{
		float L_0 = (&___a0)->get_x_1();
		float L_1 = (&___a0)->get_x_1();
		float L_2 = (&___a0)->get_y_2();
		float L_3 = (&___a0)->get_y_2();
		float L_4 = (&___a0)->get_z_3();
		float L_5 = (&___a0)->get_z_3();
		return ((float)((float)((float)((float)((float)((float)L_0*(float)L_1))+(float)((float)((float)L_2*(float)L_3))))+(float)((float)((float)L_4*(float)L_5))));
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::get_zero()
extern "C"  Vector3_t4282066566  Vector3_get_zero_m2017759730 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		Vector3_t4282066566  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Vector3__ctor_m2926210380(&L_0, (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::get_one()
extern "C"  Vector3_t4282066566  Vector3_get_one_m886467710 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		Vector3_t4282066566  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Vector3__ctor_m2926210380(&L_0, (1.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::get_forward()
extern "C"  Vector3_t4282066566  Vector3_get_forward_m1039372701 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		Vector3_t4282066566  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Vector3__ctor_m2926210380(&L_0, (0.0f), (0.0f), (1.0f), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::get_up()
extern "C"  Vector3_t4282066566  Vector3_get_up_m4046647141 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		Vector3_t4282066566  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Vector3__ctor_m2926210380(&L_0, (0.0f), (1.0f), (0.0f), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::get_left()
extern "C"  Vector3_t4282066566  Vector3_get_left_m1616598929 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		Vector3_t4282066566  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Vector3__ctor_m2926210380(&L_0, (-1.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::get_right()
extern "C"  Vector3_t4282066566  Vector3_get_right_m4015137012 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		Vector3_t4282066566  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Vector3__ctor_m2926210380(&L_0, (1.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::op_Addition(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Vector3_t4282066566  Vector3_op_Addition_m695438225 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___a0, Vector3_t4282066566  ___b1, const MethodInfo* method)
{
	{
		float L_0 = (&___a0)->get_x_1();
		float L_1 = (&___b1)->get_x_1();
		float L_2 = (&___a0)->get_y_2();
		float L_3 = (&___b1)->get_y_2();
		float L_4 = (&___a0)->get_z_3();
		float L_5 = (&___b1)->get_z_3();
		Vector3_t4282066566  L_6;
		memset(&L_6, 0, sizeof(L_6));
		Vector3__ctor_m2926210380(&L_6, ((float)((float)L_0+(float)L_1)), ((float)((float)L_2+(float)L_3)), ((float)((float)L_4+(float)L_5)), /*hidden argument*/NULL);
		return L_6;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::op_Subtraction(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Vector3_t4282066566  Vector3_op_Subtraction_m2842958165 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___a0, Vector3_t4282066566  ___b1, const MethodInfo* method)
{
	{
		float L_0 = (&___a0)->get_x_1();
		float L_1 = (&___b1)->get_x_1();
		float L_2 = (&___a0)->get_y_2();
		float L_3 = (&___b1)->get_y_2();
		float L_4 = (&___a0)->get_z_3();
		float L_5 = (&___b1)->get_z_3();
		Vector3_t4282066566  L_6;
		memset(&L_6, 0, sizeof(L_6));
		Vector3__ctor_m2926210380(&L_6, ((float)((float)L_0-(float)L_1)), ((float)((float)L_2-(float)L_3)), ((float)((float)L_4-(float)L_5)), /*hidden argument*/NULL);
		return L_6;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::op_UnaryNegation(UnityEngine.Vector3)
extern "C"  Vector3_t4282066566  Vector3_op_UnaryNegation_m3293197314 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___a0, const MethodInfo* method)
{
	{
		float L_0 = (&___a0)->get_x_1();
		float L_1 = (&___a0)->get_y_2();
		float L_2 = (&___a0)->get_z_3();
		Vector3_t4282066566  L_3;
		memset(&L_3, 0, sizeof(L_3));
		Vector3__ctor_m2926210380(&L_3, ((-L_0)), ((-L_1)), ((-L_2)), /*hidden argument*/NULL);
		return L_3;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::op_Multiply(UnityEngine.Vector3,System.Single)
extern "C"  Vector3_t4282066566  Vector3_op_Multiply_m973638459 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___a0, float ___d1, const MethodInfo* method)
{
	{
		float L_0 = (&___a0)->get_x_1();
		float L_1 = ___d1;
		float L_2 = (&___a0)->get_y_2();
		float L_3 = ___d1;
		float L_4 = (&___a0)->get_z_3();
		float L_5 = ___d1;
		Vector3_t4282066566  L_6;
		memset(&L_6, 0, sizeof(L_6));
		Vector3__ctor_m2926210380(&L_6, ((float)((float)L_0*(float)L_1)), ((float)((float)L_2*(float)L_3)), ((float)((float)L_4*(float)L_5)), /*hidden argument*/NULL);
		return L_6;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::op_Division(UnityEngine.Vector3,System.Single)
extern "C"  Vector3_t4282066566  Vector3_op_Division_m4277988370 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___a0, float ___d1, const MethodInfo* method)
{
	{
		float L_0 = (&___a0)->get_x_1();
		float L_1 = ___d1;
		float L_2 = (&___a0)->get_y_2();
		float L_3 = ___d1;
		float L_4 = (&___a0)->get_z_3();
		float L_5 = ___d1;
		Vector3_t4282066566  L_6;
		memset(&L_6, 0, sizeof(L_6));
		Vector3__ctor_m2926210380(&L_6, ((float)((float)L_0/(float)L_1)), ((float)((float)L_2/(float)L_3)), ((float)((float)L_4/(float)L_5)), /*hidden argument*/NULL);
		return L_6;
	}
}
// System.Boolean UnityEngine.Vector3::op_Inequality(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  bool Vector3_op_Inequality_m231387234 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___lhs0, Vector3_t4282066566  ___rhs1, const MethodInfo* method)
{
	{
		Vector3_t4282066566  L_0 = ___lhs0;
		Vector3_t4282066566  L_1 = ___rhs1;
		Vector3_t4282066566  L_2 = Vector3_op_Subtraction_m2842958165(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		float L_3 = Vector3_SqrMagnitude_m1662776270(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return (bool)((((int32_t)((!(((float)L_3) >= ((float)(9.99999944E-11f))))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// Conversion methods for marshalling of: UnityEngine.Vector3
extern "C" void Vector3_t4282066566_marshal_pinvoke(const Vector3_t4282066566& unmarshaled, Vector3_t4282066566_marshaled_pinvoke& marshaled)
{
	marshaled.___x_1 = unmarshaled.get_x_1();
	marshaled.___y_2 = unmarshaled.get_y_2();
	marshaled.___z_3 = unmarshaled.get_z_3();
}
extern "C" void Vector3_t4282066566_marshal_pinvoke_back(const Vector3_t4282066566_marshaled_pinvoke& marshaled, Vector3_t4282066566& unmarshaled)
{
	float unmarshaled_x_temp_0 = 0.0f;
	unmarshaled_x_temp_0 = marshaled.___x_1;
	unmarshaled.set_x_1(unmarshaled_x_temp_0);
	float unmarshaled_y_temp_1 = 0.0f;
	unmarshaled_y_temp_1 = marshaled.___y_2;
	unmarshaled.set_y_2(unmarshaled_y_temp_1);
	float unmarshaled_z_temp_2 = 0.0f;
	unmarshaled_z_temp_2 = marshaled.___z_3;
	unmarshaled.set_z_3(unmarshaled_z_temp_2);
}
// Conversion method for clean up from marshalling of: UnityEngine.Vector3
extern "C" void Vector3_t4282066566_marshal_pinvoke_cleanup(Vector3_t4282066566_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.Vector3
extern "C" void Vector3_t4282066566_marshal_com(const Vector3_t4282066566& unmarshaled, Vector3_t4282066566_marshaled_com& marshaled)
{
	marshaled.___x_1 = unmarshaled.get_x_1();
	marshaled.___y_2 = unmarshaled.get_y_2();
	marshaled.___z_3 = unmarshaled.get_z_3();
}
extern "C" void Vector3_t4282066566_marshal_com_back(const Vector3_t4282066566_marshaled_com& marshaled, Vector3_t4282066566& unmarshaled)
{
	float unmarshaled_x_temp_0 = 0.0f;
	unmarshaled_x_temp_0 = marshaled.___x_1;
	unmarshaled.set_x_1(unmarshaled_x_temp_0);
	float unmarshaled_y_temp_1 = 0.0f;
	unmarshaled_y_temp_1 = marshaled.___y_2;
	unmarshaled.set_y_2(unmarshaled_y_temp_1);
	float unmarshaled_z_temp_2 = 0.0f;
	unmarshaled_z_temp_2 = marshaled.___z_3;
	unmarshaled.set_z_3(unmarshaled_z_temp_2);
}
// Conversion method for clean up from marshalling of: UnityEngine.Vector3
extern "C" void Vector3_t4282066566_marshal_com_cleanup(Vector3_t4282066566_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.Vector4::.ctor(System.Single,System.Single,System.Single,System.Single)
extern "C"  void Vector4__ctor_m2441427762 (Vector4_t4282066567 * __this, float ___x0, float ___y1, float ___z2, float ___w3, const MethodInfo* method)
{
	{
		float L_0 = ___x0;
		__this->set_x_1(L_0);
		float L_1 = ___y1;
		__this->set_y_2(L_1);
		float L_2 = ___z2;
		__this->set_z_3(L_2);
		float L_3 = ___w3;
		__this->set_w_4(L_3);
		return;
	}
}
extern "C"  void Vector4__ctor_m2441427762_AdjustorThunk (Il2CppObject * __this, float ___x0, float ___y1, float ___z2, float ___w3, const MethodInfo* method)
{
	Vector4_t4282066567 * _thisAdjusted = reinterpret_cast<Vector4_t4282066567 *>(__this + 1);
	Vector4__ctor_m2441427762(_thisAdjusted, ___x0, ___y1, ___z2, ___w3, method);
}
// System.Int32 UnityEngine.Vector4::GetHashCode()
extern "C"  int32_t Vector4_GetHashCode_m3402333527 (Vector4_t4282066567 * __this, const MethodInfo* method)
{
	{
		float* L_0 = __this->get_address_of_x_1();
		int32_t L_1 = Single_GetHashCode_m65342520(L_0, /*hidden argument*/NULL);
		float* L_2 = __this->get_address_of_y_2();
		int32_t L_3 = Single_GetHashCode_m65342520(L_2, /*hidden argument*/NULL);
		float* L_4 = __this->get_address_of_z_3();
		int32_t L_5 = Single_GetHashCode_m65342520(L_4, /*hidden argument*/NULL);
		float* L_6 = __this->get_address_of_w_4();
		int32_t L_7 = Single_GetHashCode_m65342520(L_6, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_1^(int32_t)((int32_t)((int32_t)L_3<<(int32_t)2))))^(int32_t)((int32_t)((int32_t)L_5>>(int32_t)2))))^(int32_t)((int32_t)((int32_t)L_7>>(int32_t)1))));
	}
}
extern "C"  int32_t Vector4_GetHashCode_m3402333527_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Vector4_t4282066567 * _thisAdjusted = reinterpret_cast<Vector4_t4282066567 *>(__this + 1);
	return Vector4_GetHashCode_m3402333527(_thisAdjusted, method);
}
// System.Boolean UnityEngine.Vector4::Equals(System.Object)
extern Il2CppClass* Vector4_t4282066567_il2cpp_TypeInfo_var;
extern const uint32_t Vector4_Equals_m3270185343_MetadataUsageId;
extern "C"  bool Vector4_Equals_m3270185343 (Vector4_t4282066567 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Vector4_Equals_m3270185343_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Vector4_t4282066567  V_0;
	memset(&V_0, 0, sizeof(V_0));
	int32_t G_B7_0 = 0;
	{
		Il2CppObject * L_0 = ___other0;
		if (((Il2CppObject *)IsInstSealed(L_0, Vector4_t4282066567_il2cpp_TypeInfo_var)))
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}

IL_000d:
	{
		Il2CppObject * L_1 = ___other0;
		V_0 = ((*(Vector4_t4282066567 *)((Vector4_t4282066567 *)UnBox (L_1, Vector4_t4282066567_il2cpp_TypeInfo_var))));
		float* L_2 = __this->get_address_of_x_1();
		float L_3 = (&V_0)->get_x_1();
		bool L_4 = Single_Equals_m2110115959(L_2, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_006d;
		}
	}
	{
		float* L_5 = __this->get_address_of_y_2();
		float L_6 = (&V_0)->get_y_2();
		bool L_7 = Single_Equals_m2110115959(L_5, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_006d;
		}
	}
	{
		float* L_8 = __this->get_address_of_z_3();
		float L_9 = (&V_0)->get_z_3();
		bool L_10 = Single_Equals_m2110115959(L_8, L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_006d;
		}
	}
	{
		float* L_11 = __this->get_address_of_w_4();
		float L_12 = (&V_0)->get_w_4();
		bool L_13 = Single_Equals_m2110115959(L_11, L_12, /*hidden argument*/NULL);
		G_B7_0 = ((int32_t)(L_13));
		goto IL_006e;
	}

IL_006d:
	{
		G_B7_0 = 0;
	}

IL_006e:
	{
		return (bool)G_B7_0;
	}
}
extern "C"  bool Vector4_Equals_m3270185343_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	Vector4_t4282066567 * _thisAdjusted = reinterpret_cast<Vector4_t4282066567 *>(__this + 1);
	return Vector4_Equals_m3270185343(_thisAdjusted, ___other0, method);
}
// System.String UnityEngine.Vector4::ToString()
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern Il2CppClass* Single_t4291918972_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral843281963;
extern const uint32_t Vector4_ToString_m3272970053_MetadataUsageId;
extern "C"  String_t* Vector4_ToString_m3272970053 (Vector4_t4282066567 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Vector4_ToString_m3272970053_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t1108656482* L_0 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)4));
		float L_1 = __this->get_x_1();
		float L_2 = L_1;
		Il2CppObject * L_3 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_3);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_3);
		ObjectU5BU5D_t1108656482* L_4 = L_0;
		float L_5 = __this->get_y_2();
		float L_6 = L_5;
		Il2CppObject * L_7 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 1);
		ArrayElementTypeCheck (L_4, L_7);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_7);
		ObjectU5BU5D_t1108656482* L_8 = L_4;
		float L_9 = __this->get_z_3();
		float L_10 = L_9;
		Il2CppObject * L_11 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 2);
		ArrayElementTypeCheck (L_8, L_11);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)L_11);
		ObjectU5BU5D_t1108656482* L_12 = L_8;
		float L_13 = __this->get_w_4();
		float L_14 = L_13;
		Il2CppObject * L_15 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_14);
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 3);
		ArrayElementTypeCheck (L_12, L_15);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_15);
		String_t* L_16 = UnityString_Format_m427603113(NULL /*static, unused*/, _stringLiteral843281963, L_12, /*hidden argument*/NULL);
		return L_16;
	}
}
extern "C"  String_t* Vector4_ToString_m3272970053_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Vector4_t4282066567 * _thisAdjusted = reinterpret_cast<Vector4_t4282066567 *>(__this + 1);
	return Vector4_ToString_m3272970053(_thisAdjusted, method);
}
// UnityEngine.Vector3 UnityEngine.Vector4::op_Implicit(UnityEngine.Vector4)
extern "C"  Vector3_t4282066566  Vector4_op_Implicit_m3933247893 (Il2CppObject * __this /* static, unused */, Vector4_t4282066567  ___v0, const MethodInfo* method)
{
	{
		float L_0 = (&___v0)->get_x_1();
		float L_1 = (&___v0)->get_y_2();
		float L_2 = (&___v0)->get_z_3();
		Vector3_t4282066566  L_3;
		memset(&L_3, 0, sizeof(L_3));
		Vector3__ctor_m2926210380(&L_3, L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// Conversion methods for marshalling of: UnityEngine.Vector4
extern "C" void Vector4_t4282066567_marshal_pinvoke(const Vector4_t4282066567& unmarshaled, Vector4_t4282066567_marshaled_pinvoke& marshaled)
{
	marshaled.___x_1 = unmarshaled.get_x_1();
	marshaled.___y_2 = unmarshaled.get_y_2();
	marshaled.___z_3 = unmarshaled.get_z_3();
	marshaled.___w_4 = unmarshaled.get_w_4();
}
extern "C" void Vector4_t4282066567_marshal_pinvoke_back(const Vector4_t4282066567_marshaled_pinvoke& marshaled, Vector4_t4282066567& unmarshaled)
{
	float unmarshaled_x_temp_0 = 0.0f;
	unmarshaled_x_temp_0 = marshaled.___x_1;
	unmarshaled.set_x_1(unmarshaled_x_temp_0);
	float unmarshaled_y_temp_1 = 0.0f;
	unmarshaled_y_temp_1 = marshaled.___y_2;
	unmarshaled.set_y_2(unmarshaled_y_temp_1);
	float unmarshaled_z_temp_2 = 0.0f;
	unmarshaled_z_temp_2 = marshaled.___z_3;
	unmarshaled.set_z_3(unmarshaled_z_temp_2);
	float unmarshaled_w_temp_3 = 0.0f;
	unmarshaled_w_temp_3 = marshaled.___w_4;
	unmarshaled.set_w_4(unmarshaled_w_temp_3);
}
// Conversion method for clean up from marshalling of: UnityEngine.Vector4
extern "C" void Vector4_t4282066567_marshal_pinvoke_cleanup(Vector4_t4282066567_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.Vector4
extern "C" void Vector4_t4282066567_marshal_com(const Vector4_t4282066567& unmarshaled, Vector4_t4282066567_marshaled_com& marshaled)
{
	marshaled.___x_1 = unmarshaled.get_x_1();
	marshaled.___y_2 = unmarshaled.get_y_2();
	marshaled.___z_3 = unmarshaled.get_z_3();
	marshaled.___w_4 = unmarshaled.get_w_4();
}
extern "C" void Vector4_t4282066567_marshal_com_back(const Vector4_t4282066567_marshaled_com& marshaled, Vector4_t4282066567& unmarshaled)
{
	float unmarshaled_x_temp_0 = 0.0f;
	unmarshaled_x_temp_0 = marshaled.___x_1;
	unmarshaled.set_x_1(unmarshaled_x_temp_0);
	float unmarshaled_y_temp_1 = 0.0f;
	unmarshaled_y_temp_1 = marshaled.___y_2;
	unmarshaled.set_y_2(unmarshaled_y_temp_1);
	float unmarshaled_z_temp_2 = 0.0f;
	unmarshaled_z_temp_2 = marshaled.___z_3;
	unmarshaled.set_z_3(unmarshaled_z_temp_2);
	float unmarshaled_w_temp_3 = 0.0f;
	unmarshaled_w_temp_3 = marshaled.___w_4;
	unmarshaled.set_w_4(unmarshaled_w_temp_3);
}
// Conversion method for clean up from marshalling of: UnityEngine.Vector4
extern "C" void Vector4_t4282066567_marshal_com_cleanup(Vector4_t4282066567_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.WaitForEndOfFrame::.ctor()
extern "C"  void WaitForEndOfFrame__ctor_m4124201226 (WaitForEndOfFrame_t2372756133 * __this, const MethodInfo* method)
{
	{
		YieldInstruction__ctor_m539393484(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.WaitForFixedUpdate::.ctor()
extern "C"  void WaitForFixedUpdate__ctor_m2916734308 (WaitForFixedUpdate_t2130080621 * __this, const MethodInfo* method)
{
	{
		YieldInstruction__ctor_m539393484(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.WaitForSeconds::.ctor(System.Single)
extern "C"  void WaitForSeconds__ctor_m3184996201 (WaitForSeconds_t1615819279 * __this, float ___seconds0, const MethodInfo* method)
{
	{
		YieldInstruction__ctor_m539393484(__this, /*hidden argument*/NULL);
		float L_0 = ___seconds0;
		__this->set_m_Seconds_0(L_0);
		return;
	}
}
// Conversion methods for marshalling of: UnityEngine.WaitForSeconds
extern "C" void WaitForSeconds_t1615819279_marshal_pinvoke(const WaitForSeconds_t1615819279& unmarshaled, WaitForSeconds_t1615819279_marshaled_pinvoke& marshaled)
{
	marshaled.___m_Seconds_0 = unmarshaled.get_m_Seconds_0();
}
extern "C" void WaitForSeconds_t1615819279_marshal_pinvoke_back(const WaitForSeconds_t1615819279_marshaled_pinvoke& marshaled, WaitForSeconds_t1615819279& unmarshaled)
{
	float unmarshaled_m_Seconds_temp_0 = 0.0f;
	unmarshaled_m_Seconds_temp_0 = marshaled.___m_Seconds_0;
	unmarshaled.set_m_Seconds_0(unmarshaled_m_Seconds_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.WaitForSeconds
extern "C" void WaitForSeconds_t1615819279_marshal_pinvoke_cleanup(WaitForSeconds_t1615819279_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.WaitForSeconds
extern "C" void WaitForSeconds_t1615819279_marshal_com(const WaitForSeconds_t1615819279& unmarshaled, WaitForSeconds_t1615819279_marshaled_com& marshaled)
{
	marshaled.___m_Seconds_0 = unmarshaled.get_m_Seconds_0();
}
extern "C" void WaitForSeconds_t1615819279_marshal_com_back(const WaitForSeconds_t1615819279_marshaled_com& marshaled, WaitForSeconds_t1615819279& unmarshaled)
{
	float unmarshaled_m_Seconds_temp_0 = 0.0f;
	unmarshaled_m_Seconds_temp_0 = marshaled.___m_Seconds_0;
	unmarshaled.set_m_Seconds_0(unmarshaled_m_Seconds_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.WaitForSeconds
extern "C" void WaitForSeconds_t1615819279_marshal_com_cleanup(WaitForSeconds_t1615819279_marshaled_com& marshaled)
{
}
// System.String UnityEngine.WebCamDevice::get_name()
extern "C"  String_t* WebCamDevice_get_name_m2875559007 (WebCamDevice_t3274004757 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_m_Name_0();
		return L_0;
	}
}
extern "C"  String_t* WebCamDevice_get_name_m2875559007_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	WebCamDevice_t3274004757 * _thisAdjusted = reinterpret_cast<WebCamDevice_t3274004757 *>(__this + 1);
	return WebCamDevice_get_name_m2875559007(_thisAdjusted, method);
}
// Conversion methods for marshalling of: UnityEngine.WebCamDevice
extern "C" void WebCamDevice_t3274004757_marshal_pinvoke(const WebCamDevice_t3274004757& unmarshaled, WebCamDevice_t3274004757_marshaled_pinvoke& marshaled)
{
	marshaled.___m_Name_0 = il2cpp_codegen_marshal_string(unmarshaled.get_m_Name_0());
	marshaled.___m_Flags_1 = unmarshaled.get_m_Flags_1();
}
extern "C" void WebCamDevice_t3274004757_marshal_pinvoke_back(const WebCamDevice_t3274004757_marshaled_pinvoke& marshaled, WebCamDevice_t3274004757& unmarshaled)
{
	unmarshaled.set_m_Name_0(il2cpp_codegen_marshal_string_result(marshaled.___m_Name_0));
	int32_t unmarshaled_m_Flags_temp_1 = 0;
	unmarshaled_m_Flags_temp_1 = marshaled.___m_Flags_1;
	unmarshaled.set_m_Flags_1(unmarshaled_m_Flags_temp_1);
}
// Conversion method for clean up from marshalling of: UnityEngine.WebCamDevice
extern "C" void WebCamDevice_t3274004757_marshal_pinvoke_cleanup(WebCamDevice_t3274004757_marshaled_pinvoke& marshaled)
{
	il2cpp_codegen_marshal_free(marshaled.___m_Name_0);
	marshaled.___m_Name_0 = NULL;
}
// Conversion methods for marshalling of: UnityEngine.WebCamDevice
extern "C" void WebCamDevice_t3274004757_marshal_com(const WebCamDevice_t3274004757& unmarshaled, WebCamDevice_t3274004757_marshaled_com& marshaled)
{
	marshaled.___m_Name_0 = il2cpp_codegen_marshal_bstring(unmarshaled.get_m_Name_0());
	marshaled.___m_Flags_1 = unmarshaled.get_m_Flags_1();
}
extern "C" void WebCamDevice_t3274004757_marshal_com_back(const WebCamDevice_t3274004757_marshaled_com& marshaled, WebCamDevice_t3274004757& unmarshaled)
{
	unmarshaled.set_m_Name_0(il2cpp_codegen_marshal_bstring_result(marshaled.___m_Name_0));
	int32_t unmarshaled_m_Flags_temp_1 = 0;
	unmarshaled_m_Flags_temp_1 = marshaled.___m_Flags_1;
	unmarshaled.set_m_Flags_1(unmarshaled_m_Flags_temp_1);
}
// Conversion method for clean up from marshalling of: UnityEngine.WebCamDevice
extern "C" void WebCamDevice_t3274004757_marshal_com_cleanup(WebCamDevice_t3274004757_marshaled_com& marshaled)
{
	il2cpp_codegen_marshal_free_bstring(marshaled.___m_Name_0);
	marshaled.___m_Name_0 = NULL;
}
// System.Void UnityEngine.WebCamTexture::.ctor()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t WebCamTexture__ctor_m755637529_MetadataUsageId;
extern "C"  void WebCamTexture__ctor_m755637529 (WebCamTexture_t1290350902 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebCamTexture__ctor_m755637529_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Texture__ctor_m516856734(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		WebCamTexture_Internal_CreateWebCamTexture_m384251711(NULL /*static, unused*/, __this, L_0, 0, 0, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.WebCamTexture::Internal_CreateWebCamTexture(UnityEngine.WebCamTexture,System.String,System.Int32,System.Int32,System.Int32)
extern "C"  void WebCamTexture_Internal_CreateWebCamTexture_m384251711 (Il2CppObject * __this /* static, unused */, WebCamTexture_t1290350902 * ___self0, String_t* ___scriptingDevice1, int32_t ___requestedWidth2, int32_t ___requestedHeight3, int32_t ___maxFramerate4, const MethodInfo* method)
{
	typedef void (*WebCamTexture_Internal_CreateWebCamTexture_m384251711_ftn) (WebCamTexture_t1290350902 *, String_t*, int32_t, int32_t, int32_t);
	static WebCamTexture_Internal_CreateWebCamTexture_m384251711_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (WebCamTexture_Internal_CreateWebCamTexture_m384251711_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.WebCamTexture::Internal_CreateWebCamTexture(UnityEngine.WebCamTexture,System.String,System.Int32,System.Int32,System.Int32)");
	_il2cpp_icall_func(___self0, ___scriptingDevice1, ___requestedWidth2, ___requestedHeight3, ___maxFramerate4);
}
// System.Void UnityEngine.WebCamTexture::Play()
extern "C"  void WebCamTexture_Play_m2252445503 (WebCamTexture_t1290350902 * __this, const MethodInfo* method)
{
	{
		WebCamTexture_INTERNAL_CALL_Play_m3478980075(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.WebCamTexture::INTERNAL_CALL_Play(UnityEngine.WebCamTexture)
extern "C"  void WebCamTexture_INTERNAL_CALL_Play_m3478980075 (Il2CppObject * __this /* static, unused */, WebCamTexture_t1290350902 * ___self0, const MethodInfo* method)
{
	typedef void (*WebCamTexture_INTERNAL_CALL_Play_m3478980075_ftn) (WebCamTexture_t1290350902 *);
	static WebCamTexture_INTERNAL_CALL_Play_m3478980075_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (WebCamTexture_INTERNAL_CALL_Play_m3478980075_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.WebCamTexture::INTERNAL_CALL_Play(UnityEngine.WebCamTexture)");
	_il2cpp_icall_func(___self0);
}
// System.Void UnityEngine.WebCamTexture::Stop()
extern "C"  void WebCamTexture_Stop_m2346129549 (WebCamTexture_t1290350902 * __this, const MethodInfo* method)
{
	{
		WebCamTexture_INTERNAL_CALL_Stop_m3733059677(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.WebCamTexture::INTERNAL_CALL_Stop(UnityEngine.WebCamTexture)
extern "C"  void WebCamTexture_INTERNAL_CALL_Stop_m3733059677 (Il2CppObject * __this /* static, unused */, WebCamTexture_t1290350902 * ___self0, const MethodInfo* method)
{
	typedef void (*WebCamTexture_INTERNAL_CALL_Stop_m3733059677_ftn) (WebCamTexture_t1290350902 *);
	static WebCamTexture_INTERNAL_CALL_Stop_m3733059677_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (WebCamTexture_INTERNAL_CALL_Stop_m3733059677_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.WebCamTexture::INTERNAL_CALL_Stop(UnityEngine.WebCamTexture)");
	_il2cpp_icall_func(___self0);
}
// System.Boolean UnityEngine.WebCamTexture::get_isPlaying()
extern "C"  bool WebCamTexture_get_isPlaying_m1109067512 (WebCamTexture_t1290350902 * __this, const MethodInfo* method)
{
	typedef bool (*WebCamTexture_get_isPlaying_m1109067512_ftn) (WebCamTexture_t1290350902 *);
	static WebCamTexture_get_isPlaying_m1109067512_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (WebCamTexture_get_isPlaying_m1109067512_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.WebCamTexture::get_isPlaying()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.WebCamTexture::set_deviceName(System.String)
extern "C"  void WebCamTexture_set_deviceName_m1209257657 (WebCamTexture_t1290350902 * __this, String_t* ___value0, const MethodInfo* method)
{
	typedef void (*WebCamTexture_set_deviceName_m1209257657_ftn) (WebCamTexture_t1290350902 *, String_t*);
	static WebCamTexture_set_deviceName_m1209257657_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (WebCamTexture_set_deviceName_m1209257657_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.WebCamTexture::set_deviceName(System.String)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.WebCamTexture::set_requestedFPS(System.Single)
extern "C"  void WebCamTexture_set_requestedFPS_m644257608 (WebCamTexture_t1290350902 * __this, float ___value0, const MethodInfo* method)
{
	typedef void (*WebCamTexture_set_requestedFPS_m644257608_ftn) (WebCamTexture_t1290350902 *, float);
	static WebCamTexture_set_requestedFPS_m644257608_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (WebCamTexture_set_requestedFPS_m644257608_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.WebCamTexture::set_requestedFPS(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.WebCamTexture::set_requestedWidth(System.Int32)
extern "C"  void WebCamTexture_set_requestedWidth_m3687101425 (WebCamTexture_t1290350902 * __this, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*WebCamTexture_set_requestedWidth_m3687101425_ftn) (WebCamTexture_t1290350902 *, int32_t);
	static WebCamTexture_set_requestedWidth_m3687101425_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (WebCamTexture_set_requestedWidth_m3687101425_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.WebCamTexture::set_requestedWidth(System.Int32)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.WebCamTexture::set_requestedHeight(System.Int32)
extern "C"  void WebCamTexture_set_requestedHeight_m3951846656 (WebCamTexture_t1290350902 * __this, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*WebCamTexture_set_requestedHeight_m3951846656_ftn) (WebCamTexture_t1290350902 *, int32_t);
	static WebCamTexture_set_requestedHeight_m3951846656_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (WebCamTexture_set_requestedHeight_m3951846656_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.WebCamTexture::set_requestedHeight(System.Int32)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.WebCamDevice[] UnityEngine.WebCamTexture::get_devices()
extern "C"  WebCamDeviceU5BU5D_t3721690872* WebCamTexture_get_devices_m3738445840 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef WebCamDeviceU5BU5D_t3721690872* (*WebCamTexture_get_devices_m3738445840_ftn) ();
	static WebCamTexture_get_devices_m3738445840_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (WebCamTexture_get_devices_m3738445840_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.WebCamTexture::get_devices()");
	return _il2cpp_icall_func();
}
// System.Boolean UnityEngine.WebCamTexture::get_didUpdateThisFrame()
extern "C"  bool WebCamTexture_get_didUpdateThisFrame_m2572205429 (WebCamTexture_t1290350902 * __this, const MethodInfo* method)
{
	typedef bool (*WebCamTexture_get_didUpdateThisFrame_m2572205429_ftn) (WebCamTexture_t1290350902 *);
	static WebCamTexture_get_didUpdateThisFrame_m2572205429_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (WebCamTexture_get_didUpdateThisFrame_m2572205429_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.WebCamTexture::get_didUpdateThisFrame()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.WrapperlessIcall::.ctor()
extern "C"  void WrapperlessIcall__ctor_m1888400594 (WrapperlessIcall_t2494346367 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m2985353781(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.WritableAttribute::.ctor()
extern "C"  void WritableAttribute__ctor_m2205809533 (WritableAttribute_t2171443922 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m2985353781(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.YieldInstruction::.ctor()
extern "C"  void YieldInstruction__ctor_m539393484 (YieldInstruction_t2048002629 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// Conversion methods for marshalling of: UnityEngine.YieldInstruction
extern "C" void YieldInstruction_t2048002629_marshal_pinvoke(const YieldInstruction_t2048002629& unmarshaled, YieldInstruction_t2048002629_marshaled_pinvoke& marshaled)
{
}
extern "C" void YieldInstruction_t2048002629_marshal_pinvoke_back(const YieldInstruction_t2048002629_marshaled_pinvoke& marshaled, YieldInstruction_t2048002629& unmarshaled)
{
}
// Conversion method for clean up from marshalling of: UnityEngine.YieldInstruction
extern "C" void YieldInstruction_t2048002629_marshal_pinvoke_cleanup(YieldInstruction_t2048002629_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.YieldInstruction
extern "C" void YieldInstruction_t2048002629_marshal_com(const YieldInstruction_t2048002629& unmarshaled, YieldInstruction_t2048002629_marshaled_com& marshaled)
{
}
extern "C" void YieldInstruction_t2048002629_marshal_com_back(const YieldInstruction_t2048002629_marshaled_com& marshaled, YieldInstruction_t2048002629& unmarshaled)
{
}
// Conversion method for clean up from marshalling of: UnityEngine.YieldInstruction
extern "C" void YieldInstruction_t2048002629_marshal_com_cleanup(YieldInstruction_t2048002629_marshaled_com& marshaled)
{
}
// System.Void UnityEngineInternal.GenericStack::.ctor()
extern "C"  void GenericStack__ctor_m2328546233 (GenericStack_t931085639 * __this, const MethodInfo* method)
{
	{
		Stack__ctor_m1821673314(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngineInternal.MathfInternal::.cctor()
extern Il2CppClass* MathfInternal_t4096243933_il2cpp_TypeInfo_var;
extern const uint32_t MathfInternal__cctor_m2600550988_MetadataUsageId;
extern "C"  void MathfInternal__cctor_m2600550988 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MathfInternal__cctor_m2600550988_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		il2cpp_codegen_memory_barrier();
		((MathfInternal_t4096243933_StaticFields*)MathfInternal_t4096243933_il2cpp_TypeInfo_var->static_fields)->set_FloatMinNormal_0((1.17549435E-38f));
		il2cpp_codegen_memory_barrier();
		((MathfInternal_t4096243933_StaticFields*)MathfInternal_t4096243933_il2cpp_TypeInfo_var->static_fields)->set_FloatMinDenormal_1((1.401298E-45f));
		((MathfInternal_t4096243933_StaticFields*)MathfInternal_t4096243933_il2cpp_TypeInfo_var->static_fields)->set_IsFlushToZeroEnabled_2((bool)1);
		return;
	}
}
// Conversion methods for marshalling of: UnityEngineInternal.MathfInternal
extern "C" void MathfInternal_t4096243933_marshal_pinvoke(const MathfInternal_t4096243933& unmarshaled, MathfInternal_t4096243933_marshaled_pinvoke& marshaled)
{
}
extern "C" void MathfInternal_t4096243933_marshal_pinvoke_back(const MathfInternal_t4096243933_marshaled_pinvoke& marshaled, MathfInternal_t4096243933& unmarshaled)
{
}
// Conversion method for clean up from marshalling of: UnityEngineInternal.MathfInternal
extern "C" void MathfInternal_t4096243933_marshal_pinvoke_cleanup(MathfInternal_t4096243933_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngineInternal.MathfInternal
extern "C" void MathfInternal_t4096243933_marshal_com(const MathfInternal_t4096243933& unmarshaled, MathfInternal_t4096243933_marshaled_com& marshaled)
{
}
extern "C" void MathfInternal_t4096243933_marshal_com_back(const MathfInternal_t4096243933_marshaled_com& marshaled, MathfInternal_t4096243933& unmarshaled)
{
}
// Conversion method for clean up from marshalling of: UnityEngineInternal.MathfInternal
extern "C" void MathfInternal_t4096243933_marshal_com_cleanup(MathfInternal_t4096243933_marshaled_com& marshaled)
{
}
// System.Void UnityEngineInternal.TypeInferenceRuleAttribute::.ctor(UnityEngineInternal.TypeInferenceRules)
extern Il2CppClass* TypeInferenceRules_t2889237774_il2cpp_TypeInfo_var;
extern const uint32_t TypeInferenceRuleAttribute__ctor_m1168575159_MetadataUsageId;
extern "C"  void TypeInferenceRuleAttribute__ctor_m1168575159 (TypeInferenceRuleAttribute_t1657757719 * __this, int32_t ___rule0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TypeInferenceRuleAttribute__ctor_m1168575159_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___rule0;
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(TypeInferenceRules_t2889237774_il2cpp_TypeInfo_var, &L_1);
		NullCheck((Enum_t2862688501 *)L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Enum::ToString() */, (Enum_t2862688501 *)L_2);
		TypeInferenceRuleAttribute__ctor_m2173394041(__this, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngineInternal.TypeInferenceRuleAttribute::.ctor(System.String)
extern "C"  void TypeInferenceRuleAttribute__ctor_m2173394041 (TypeInferenceRuleAttribute_t1657757719 * __this, String_t* ___rule0, const MethodInfo* method)
{
	{
		Attribute__ctor_m2985353781(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___rule0;
		__this->set__rule_0(L_0);
		return;
	}
}
// System.String UnityEngineInternal.TypeInferenceRuleAttribute::ToString()
extern "C"  String_t* TypeInferenceRuleAttribute_ToString_m318752778 (TypeInferenceRuleAttribute_t1657757719 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get__rule_0();
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
