﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<Vuforia.VuforiaManagerImpl/IdPair>
struct List_1_t595805021;
// System.Collections.Generic.IEnumerable`1<Vuforia.VuforiaManagerImpl/IdPair>
struct IEnumerable_1_t2528532426;
// System.Collections.Generic.IEnumerator`1<Vuforia.VuforiaManagerImpl/IdPair>
struct IEnumerator_1_t1139484518;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.ICollection`1<Vuforia.VuforiaManagerImpl/IdPair>
struct ICollection_1_t122209456;
// Vuforia.VuforiaManagerImpl/IdPair[]
struct IdPairU5BU5D_t296135328;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Object4170816371.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl3522586765.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat615477791.h"

// System.Void System.Collections.Generic.List`1<Vuforia.VuforiaManagerImpl/IdPair>::.ctor()
extern "C"  void List_1__ctor_m1515543171_gshared (List_1_t595805021 * __this, const MethodInfo* method);
#define List_1__ctor_m1515543171(__this, method) ((  void (*) (List_1_t595805021 *, const MethodInfo*))List_1__ctor_m1515543171_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VuforiaManagerImpl/IdPair>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1__ctor_m3521223093_gshared (List_1_t595805021 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1__ctor_m3521223093(__this, ___collection0, method) ((  void (*) (List_1_t595805021 *, Il2CppObject*, const MethodInfo*))List_1__ctor_m3521223093_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VuforiaManagerImpl/IdPair>::.ctor(System.Int32)
extern "C"  void List_1__ctor_m3065537364_gshared (List_1_t595805021 * __this, int32_t ___capacity0, const MethodInfo* method);
#define List_1__ctor_m3065537364(__this, ___capacity0, method) ((  void (*) (List_1_t595805021 *, int32_t, const MethodInfo*))List_1__ctor_m3065537364_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VuforiaManagerImpl/IdPair>::.cctor()
extern "C"  void List_1__cctor_m3550069130_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m3550069130(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))List_1__cctor_m3550069130_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<Vuforia.VuforiaManagerImpl/IdPair>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2901349141_gshared (List_1_t595805021 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2901349141(__this, method) ((  Il2CppObject* (*) (List_1_t595805021 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2901349141_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VuforiaManagerImpl/IdPair>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void List_1_System_Collections_ICollection_CopyTo_m3591100449_gshared (List_1_t595805021 * __this, Il2CppArray * ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m3591100449(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t595805021 *, Il2CppArray *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m3591100449_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<Vuforia.VuforiaManagerImpl/IdPair>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * List_1_System_Collections_IEnumerable_GetEnumerator_m1068006492_gshared (List_1_t595805021 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m1068006492(__this, method) ((  Il2CppObject * (*) (List_1_t595805021 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m1068006492_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.VuforiaManagerImpl/IdPair>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_Add_m3528411413_gshared (List_1_t595805021 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m3528411413(__this, ___item0, method) ((  int32_t (*) (List_1_t595805021 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Add_m3528411413_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.VuforiaManagerImpl/IdPair>::System.Collections.IList.Contains(System.Object)
extern "C"  bool List_1_System_Collections_IList_Contains_m350287063_gshared (List_1_t595805021 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m350287063(__this, ___item0, method) ((  bool (*) (List_1_t595805021 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Contains_m350287063_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.VuforiaManagerImpl/IdPair>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_IndexOf_m3354062061_gshared (List_1_t595805021 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m3354062061(__this, ___item0, method) ((  int32_t (*) (List_1_t595805021 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m3354062061_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VuforiaManagerImpl/IdPair>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_Insert_m2604325784_gshared (List_1_t595805021 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m2604325784(__this, ___index0, ___item1, method) ((  void (*) (List_1_t595805021 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Insert_m2604325784_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VuforiaManagerImpl/IdPair>::System.Collections.IList.Remove(System.Object)
extern "C"  void List_1_System_Collections_IList_Remove_m2351158928_gshared (List_1_t595805021 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m2351158928(__this, ___item0, method) ((  void (*) (List_1_t595805021 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Remove_m2351158928_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.VuforiaManagerImpl/IdPair>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m587072536_gshared (List_1_t595805021 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m587072536(__this, method) ((  bool (*) (List_1_t595805021 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m587072536_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Vuforia.VuforiaManagerImpl/IdPair>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * List_1_System_Collections_ICollection_get_SyncRoot_m3224091729_gshared (List_1_t595805021 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m3224091729(__this, method) ((  Il2CppObject * (*) (List_1_t595805021 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m3224091729_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Vuforia.VuforiaManagerImpl/IdPair>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * List_1_System_Collections_IList_get_Item_m2746342808_gshared (List_1_t595805021 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m2746342808(__this, ___index0, method) ((  Il2CppObject * (*) (List_1_t595805021 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m2746342808_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VuforiaManagerImpl/IdPair>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_set_Item_m3700064623_gshared (List_1_t595805021 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m3700064623(__this, ___index0, ___value1, method) ((  void (*) (List_1_t595805021 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m3700064623_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VuforiaManagerImpl/IdPair>::Add(T)
extern "C"  void List_1_Add_m2671430044_gshared (List_1_t595805021 * __this, IdPair_t3522586765  ___item0, const MethodInfo* method);
#define List_1_Add_m2671430044(__this, ___item0, method) ((  void (*) (List_1_t595805021 *, IdPair_t3522586765 , const MethodInfo*))List_1_Add_m2671430044_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VuforiaManagerImpl/IdPair>::GrowIfNeeded(System.Int32)
extern "C"  void List_1_GrowIfNeeded_m803541463_gshared (List_1_t595805021 * __this, int32_t ___newCount0, const MethodInfo* method);
#define List_1_GrowIfNeeded_m803541463(__this, ___newCount0, method) ((  void (*) (List_1_t595805021 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m803541463_gshared)(__this, ___newCount0, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VuforiaManagerImpl/IdPair>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C"  void List_1_AddCollection_m2357218517_gshared (List_1_t595805021 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddCollection_m2357218517(__this, ___collection0, method) ((  void (*) (List_1_t595805021 *, Il2CppObject*, const MethodInfo*))List_1_AddCollection_m2357218517_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VuforiaManagerImpl/IdPair>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddEnumerable_m1618190741_gshared (List_1_t595805021 * __this, Il2CppObject* ___enumerable0, const MethodInfo* method);
#define List_1_AddEnumerable_m1618190741(__this, ___enumerable0, method) ((  void (*) (List_1_t595805021 *, Il2CppObject*, const MethodInfo*))List_1_AddEnumerable_m1618190741_gshared)(__this, ___enumerable0, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VuforiaManagerImpl/IdPair>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddRange_m1293542370_gshared (List_1_t595805021 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddRange_m1293542370(__this, ___collection0, method) ((  void (*) (List_1_t595805021 *, Il2CppObject*, const MethodInfo*))List_1_AddRange_m1293542370_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VuforiaManagerImpl/IdPair>::Clear()
extern "C"  void List_1_Clear_m3216643758_gshared (List_1_t595805021 * __this, const MethodInfo* method);
#define List_1_Clear_m3216643758(__this, method) ((  void (*) (List_1_t595805021 *, const MethodInfo*))List_1_Clear_m3216643758_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.VuforiaManagerImpl/IdPair>::Contains(T)
extern "C"  bool List_1_Contains_m823149020_gshared (List_1_t595805021 * __this, IdPair_t3522586765  ___item0, const MethodInfo* method);
#define List_1_Contains_m823149020(__this, ___item0, method) ((  bool (*) (List_1_t595805021 *, IdPair_t3522586765 , const MethodInfo*))List_1_Contains_m823149020_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VuforiaManagerImpl/IdPair>::CopyTo(T[],System.Int32)
extern "C"  void List_1_CopyTo_m3967025804_gshared (List_1_t595805021 * __this, IdPairU5BU5D_t296135328* ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_CopyTo_m3967025804(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t595805021 *, IdPairU5BU5D_t296135328*, int32_t, const MethodInfo*))List_1_CopyTo_m3967025804_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<Vuforia.VuforiaManagerImpl/IdPair>::GetEnumerator()
extern "C"  Enumerator_t615477791  List_1_GetEnumerator_m1885753824_gshared (List_1_t595805021 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m1885753824(__this, method) ((  Enumerator_t615477791  (*) (List_1_t595805021 *, const MethodInfo*))List_1_GetEnumerator_m1885753824_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.VuforiaManagerImpl/IdPair>::IndexOf(T)
extern "C"  int32_t List_1_IndexOf_m3627519760_gshared (List_1_t595805021 * __this, IdPair_t3522586765  ___item0, const MethodInfo* method);
#define List_1_IndexOf_m3627519760(__this, ___item0, method) ((  int32_t (*) (List_1_t595805021 *, IdPair_t3522586765 , const MethodInfo*))List_1_IndexOf_m3627519760_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VuforiaManagerImpl/IdPair>::Shift(System.Int32,System.Int32)
extern "C"  void List_1_Shift_m1125551331_gshared (List_1_t595805021 * __this, int32_t ___start0, int32_t ___delta1, const MethodInfo* method);
#define List_1_Shift_m1125551331(__this, ___start0, ___delta1, method) ((  void (*) (List_1_t595805021 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m1125551331_gshared)(__this, ___start0, ___delta1, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VuforiaManagerImpl/IdPair>::CheckIndex(System.Int32)
extern "C"  void List_1_CheckIndex_m3713392988_gshared (List_1_t595805021 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_CheckIndex_m3713392988(__this, ___index0, method) ((  void (*) (List_1_t595805021 *, int32_t, const MethodInfo*))List_1_CheckIndex_m3713392988_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VuforiaManagerImpl/IdPair>::Insert(System.Int32,T)
extern "C"  void List_1_Insert_m2953399299_gshared (List_1_t595805021 * __this, int32_t ___index0, IdPair_t3522586765  ___item1, const MethodInfo* method);
#define List_1_Insert_m2953399299(__this, ___index0, ___item1, method) ((  void (*) (List_1_t595805021 *, int32_t, IdPair_t3522586765 , const MethodInfo*))List_1_Insert_m2953399299_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VuforiaManagerImpl/IdPair>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_CheckCollection_m2365898296_gshared (List_1_t595805021 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_CheckCollection_m2365898296(__this, ___collection0, method) ((  void (*) (List_1_t595805021 *, Il2CppObject*, const MethodInfo*))List_1_CheckCollection_m2365898296_gshared)(__this, ___collection0, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.VuforiaManagerImpl/IdPair>::Remove(T)
extern "C"  bool List_1_Remove_m4192454871_gshared (List_1_t595805021 * __this, IdPair_t3522586765  ___item0, const MethodInfo* method);
#define List_1_Remove_m4192454871(__this, ___item0, method) ((  bool (*) (List_1_t595805021 *, IdPair_t3522586765 , const MethodInfo*))List_1_Remove_m4192454871_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VuforiaManagerImpl/IdPair>::RemoveAt(System.Int32)
extern "C"  void List_1_RemoveAt_m827252169_gshared (List_1_t595805021 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_RemoveAt_m827252169(__this, ___index0, method) ((  void (*) (List_1_t595805021 *, int32_t, const MethodInfo*))List_1_RemoveAt_m827252169_gshared)(__this, ___index0, method)
// T[] System.Collections.Generic.List`1<Vuforia.VuforiaManagerImpl/IdPair>::ToArray()
extern "C"  IdPairU5BU5D_t296135328* List_1_ToArray_m3391953794_gshared (List_1_t595805021 * __this, const MethodInfo* method);
#define List_1_ToArray_m3391953794(__this, method) ((  IdPairU5BU5D_t296135328* (*) (List_1_t595805021 *, const MethodInfo*))List_1_ToArray_m3391953794_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.VuforiaManagerImpl/IdPair>::get_Capacity()
extern "C"  int32_t List_1_get_Capacity_m2765717312_gshared (List_1_t595805021 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m2765717312(__this, method) ((  int32_t (*) (List_1_t595805021 *, const MethodInfo*))List_1_get_Capacity_m2765717312_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VuforiaManagerImpl/IdPair>::set_Capacity(System.Int32)
extern "C"  void List_1_set_Capacity_m2308697577_gshared (List_1_t595805021 * __this, int32_t ___value0, const MethodInfo* method);
#define List_1_set_Capacity_m2308697577(__this, ___value0, method) ((  void (*) (List_1_t595805021 *, int32_t, const MethodInfo*))List_1_set_Capacity_m2308697577_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.VuforiaManagerImpl/IdPair>::get_Count()
extern "C"  int32_t List_1_get_Count_m601270379_gshared (List_1_t595805021 * __this, const MethodInfo* method);
#define List_1_get_Count_m601270379(__this, method) ((  int32_t (*) (List_1_t595805021 *, const MethodInfo*))List_1_get_Count_m601270379_gshared)(__this, method)
// T System.Collections.Generic.List`1<Vuforia.VuforiaManagerImpl/IdPair>::get_Item(System.Int32)
extern "C"  IdPair_t3522586765  List_1_get_Item_m1578029517_gshared (List_1_t595805021 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_get_Item_m1578029517(__this, ___index0, method) ((  IdPair_t3522586765  (*) (List_1_t595805021 *, int32_t, const MethodInfo*))List_1_get_Item_m1578029517_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VuforiaManagerImpl/IdPair>::set_Item(System.Int32,T)
extern "C"  void List_1_set_Item_m1052940314_gshared (List_1_t595805021 * __this, int32_t ___index0, IdPair_t3522586765  ___value1, const MethodInfo* method);
#define List_1_set_Item_m1052940314(__this, ___index0, ___value1, method) ((  void (*) (List_1_t595805021 *, int32_t, IdPair_t3522586765 , const MethodInfo*))List_1_set_Item_m1052940314_gshared)(__this, ___index0, ___value1, method)
